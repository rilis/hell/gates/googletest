## Google test framework proxy repository for hell

This is proxy repository for [gtest and gmock libraries](https://github.com/google/googletest/), which allow you to build and install them using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of google test framework using hell, have improvement idea, or want to request support for other versions of google test framework, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in google test framework itself please create issue on [google test framework issue tracker](https://github.com/google/googletest/issues), because here we don't do any kind of google test framework development.